<?php

use App\Http\Controllers\Dashboard\LoginController;
use App\Http\Controllers\Dashboard\ProfileController;
use App\Http\Controllers\Dashboard\UserController;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here we register routes for admin panel/dashboard that will be consumed
| by users (admins) who have admin access.
|
*/

// Guest authentication routes
Route::middleware(['guest:admin'])->group(function () {
    Route::post('login', [LoginController::class, 'authenticate'])->name('admin.login');
    Route::post('forgot-password', [LoginController::class, 'forgotPassword'])->name('admin.forgot-password');
    Route::post('reset-password', [LoginController::class, 'resetPassword'])->name('admin.reset-password');
});

// Routes for authorized admins
Route::middleware(['auth:admin'])->group(function () {
    Route::get('logout', [LoginController::class, 'logout'])->name('admin.logout');

    // Current user profile
    Route::prefix('profile')->group(function () {
        Route::get('/', [ProfileController::class, 'index'])->name('admin.profile.index');
        Route::post('password', [ProfileController::class, 'updatePassword'])->name('admin.profile.update-password');
        Route::post('information', [ProfileController::class, 'updateInfo'])->name('admin.profile.update-info');
        Route::delete('photo', [ProfileController::class, 'deletePhoto'])->name('admin.profile.delete-photo');
        Route::delete('other-browser-sessions', [ProfileController::class, 'deleteOtherSessions'])->name('admin.profile.delete-sessions');
    });

    // User index & management
    Route::resource('users', UserController::class, [
        'names' => [
            'index'   => 'admin.users.index',
            'store'   => 'admin.users.store',
            'show'    => 'admin.users.show',
            'update'  => 'admin.users.update',
            'destroy' => 'admin.users.destroy'
        ]
    ])->except(['edit', 'create']);
});
