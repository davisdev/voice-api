<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ConversationController;
use App\Http\Controllers\DeviceController;
use App\Http\Controllers\DeviceTokenController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

// Authentication routes (for guests)
Route::middleware(['throttle:auth_attempts'])->group(function () {
    Route::post('auth/phone', [AuthController::class, 'register'])->name('login');
    Route::post('auth/phone/verify', [AuthController::class, 'verify'])->name('verify');
    Route::post('auth/phone/resend', [AuthController::class, 'resend'])->name('resend');
});

// Application routes (for authorized)
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('user', [UserController::class, 'index']);
    Route::post('user', [UserController::class, 'update']);
    Route::delete('user', [UserController::class, 'destroy']);

    // Any user data
    Route::get('users/{user}', [UserController::class, 'getUserData'])->name('index.user');

    Route::post('user/status', [StatusController::class, 'store'])->name('status.store');

    Route::post('auth/logout', [AuthController::class, 'logout'])->name('logout');

    // Online statuses
    Route::get('online', [ActivityController::class, 'index'])->name('online.listeners');
    Route::post('online', [ActivityController::class, 'store'])->name('online.broadcast');

    // Device data
    Route::post('devices', [DeviceController::class, 'store'])->name('device.store');
    Route::delete('devices/{device}', [DeviceController::class, 'destroy'])->name('device.destroy');

    // Topics
    Route::get('topics', [TopicController::class, 'index'])->name('topic.index');

    // Calls
    Route::group(['prefix' => 'call'], function () {
        Route::post('/', [ConversationController::class, 'start'])->name('call.start');
        Route::post('accept', [ConversationController::class, 'accept'])->name('call.accept');
        Route::post('cancel', [ConversationController::class, 'cancel'])->name('call.cancel');
        Route::post('finish', [ConversationController::class, 'finish'])->name('call.finish');
    });

    // Firebase device tokens
    Route::post('device-token/refresh', DeviceTokenController::class)->name('device-token.refresh');

    // Ratings
    Route::post('rating/{user}', [RatingController::class, 'store'])->name('rating.store');
});

Route::fallback(function () {
    return response()->json([
        'message' => 'Page Not Found. If error persists, contact davis@ccstudio.com.'], 404);
});
