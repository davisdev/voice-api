<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'full_name'     => 'required|string|min:3',
            'email'         => 'required|email',
            'date_of_birth' => 'date|date_format:d.m.Y',
            'gender'        => 'required|in:male,female',
            'status'        => 'in:online,offline,on-call',
            'bio'           => 'min:3'
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'gender.in' => 'User can be either male or female'
        ];
    }
}
