<?php

namespace App\Http\Requests;

use App\Http\Controllers\AuthController;
use App\Services\PhoneNumberService;
use Illuminate\Foundation\Http\FormRequest;

class VerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Adjust incoming data before validation.
     */
    public function prepareForValidation()
    {
        $this->merge([
            'phone_number' => PhoneNumberService::optimizePhoneNumber(
                $this->get('phone_number')
            )
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone_number'      => 'required|string|exists:users,phone_number',
            'verification_code' => 'required|int'
        ];
    }
}
