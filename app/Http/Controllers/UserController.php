<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserController extends Controller
{
    /**
     * Get current user profile info.
     *
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request): JsonResource
    {
        return UserResource::make(
            $request->user()
        );
    }

    /**
     * Get data about any user.
     *
     * @param User $user
     * @return JsonResource
     */
    public function getUserData(User $user): JsonResource
    {
        return UserResource::make($user);
    }

    /**
     * Update current user profile.
     *
     * @param UserUpdateRequest $request
     * @return UserResource
     */
    public function update(UserUpdateRequest $request): UserResource
    {
        $user = tap(auth()->user())->update($request->all());

        return UserResource::make(
            $user
        )->additional([
            'success' => true,
            'message' => 'User successfully updated'
        ]);
    }

    /**
     * Delete current user profile.
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(): JsonResponse
    {
        if (! auth()->user()->delete()) {
            throw new \Exception('There was a problem deleting your profile');
        }

        return response()->json([
            'success' => true,
            'message' => 'User successfully deleted',
            'data'    => []
        ]);
    }
}
