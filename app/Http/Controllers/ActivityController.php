<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ActivityController extends Controller
{
    /**
     * @return UserCollection
     */
    public function index(): ResourceCollection
    {
        return UserCollection::make(
            $this->getOnlineUsers()
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $this->recordOnlineStatus($request->user());

        return response()->json([]);
    }

    /**
     * @return LengthAwarePaginator
     */
    private function getOnlineUsers(): LengthAwarePaginator
    {
        return User::listeners()
            ->online()
            ->select(['id', 'full_name', 'rating'])
            ->addSelect(['comment' => Rating::select('comment')
                ->whereColumn('recipient_id', 'users.id')
                ->latest()
                ->take(1)
            ])
            ->paginate((int) config('administrator.pagination.default') ?? 15);
    }

    /**
     * @param Authenticatable|User $user
     *
     * @return mixed
     */
    private function recordOnlineStatus(Authenticatable|User $user): User
    {
        return tap($user)->update(['status' => 'online']);
    }
}
