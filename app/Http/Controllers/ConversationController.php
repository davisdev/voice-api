<?php

namespace App\Http\Controllers;

use App\Events\Calls\CallCancelled;
use App\Events\Calls\CallFinished;
use App\Events\Calls\CallOngoing;
use App\Exceptions\ConversationException;
use App\Exceptions\PushNotificationException;
use App\Http\Requests\Calls\AcceptCallRequest;
use App\Http\Requests\Calls\CancelCallRequest;
use App\Http\Requests\Calls\FinishCallRequest;
use App\Http\Requests\Calls\StartCallRequest;
use App\Models\Conversation;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;

class ConversationController extends Controller
{
    const NOTIF_CALL_INCOMING = 'notification_call_incoming';
    const NOTIF_CALL_CANCELLED = 'notification_call_cancelled';
    const NOTIF_CALL_FINISHED = 'notification_call_finished';

    /**
     * @param StartCallRequest $request
     * @return JsonResponse
     * @throws ConversationException
     * @throws PushNotificationException
     */
    public function start(StartCallRequest $request): JsonResponse
    {
        $listener = User::find($request->get('listener_id'));
        $conversation = Conversation::start(auth()->guard('sanctum')->user(), $listener);

        /** @var User $caller */
        $caller = $request->user();

        $this->sendPushNotification(
            $listener->device_token_listener,
            array_merge(
                $conversation->only(['id', 'channel', 'token']),
                [
                    'type'   => self::NOTIF_CALL_INCOMING,
                    'caller' => $caller->only(['full_name', 'gender', 'age'])
                ]
            ),
            title: "Ienākošs zvans no {$caller->full_name}"
        );

        return response()->json([
            'success' => true,
            'message' => 'Call has been initiated',
            'data'    => [
                'conversation_id' => $conversation->id,
                'channel'         => $conversation->channel(),
                'token'           => $conversation->token()
            ]
        ]);
    }

    /**
     * @param AcceptCallRequest $request
     * @return JsonResponse
     */
    public function accept(AcceptCallRequest $request): JsonResponse
    {
        $conversation = Conversation::find($request->get('conversation_id'));
        $conversation->markAsOngoing();

        event(new CallOngoing($conversation->fresh()));

        $request->user()->update([
            'status' => User::STATUS_ON_CALL
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Call has been accepted',
            'data'    => []
        ]);
    }

    /**
     * @param CancelCallRequest $request
     * @return JsonResponse
     * @throws ConversationException|PushNotificationException
     */
    public function cancel(CancelCallRequest $request): JsonResponse
    {
        $conversation = Conversation::find($request->get('conversation_id'));

        if (! $conversation->isCancellable()) {
            throw ConversationException::callCannotBeCancelled();
        }

        $conversation->markAsCancelled();

        event(new CallCancelled($conversation->fresh()));

        $listener = User::find($conversation->listener_id);
        $this->sendPushNotification(
            $listener->device_token_listener,
            $conversation->only(['id', 'channel', 'token']) + ['type' => self::NOTIF_CALL_CANCELLED],
            title: "Zvans pārtraukts"
        );

        return response()->json([
            'success' => true,
            'message' => 'Call has been cancelled',
            'data'    => []
        ]);
    }

    /**
     * @param FinishCallRequest $request
     * @return JsonResponse
     * @throws ConversationException|PushNotificationException
     */
    public function finish(FinishCallRequest $request): JsonResponse
    {
        $conversation = Conversation::findOrFail($request->get('conversation_id'));

        if (! $conversation->isOngoing()) {
            throw ConversationException::callNotActive();
        }

        $conversation->markAsFinished();

        event(new CallFinished($conversation->fresh()));

        User::find($conversation->listener_id)->update(['status' => User::STATUS_ONLINE]);
        $customer = User::find($conversation->caller_id);

        $this->sendPushNotification(
            $customer->device_token_customer,
            $conversation->only(['id', 'channel', 'token']) + ['type' => self::NOTIF_CALL_FINISHED],
            title: "Zvans pabeigts"
        );

        return response()->json([
            'success' => true,
            'message' => 'Call successfully finished',
            'data'    => []
        ]);
    }

    /**
     * Send push notification to Firebase.
     *
     * @param string|null $deviceToken
     * @param array $data
     * @param string $title
     * @param string $body
     * @return mixed
     * @throws PushNotificationException
     */
    private function sendPushNotification(?string $deviceToken, array $data, string $title = 'Ienākošs zvans', string $body = 'Spied, lai atvērtu aplikāciju'): bool
    {
        if (! $deviceToken) {
            throw PushNotificationException::deviceTokenNotSet();
        }

        $url = config('firebase.url');
        $apiKey = $this->determineApiKey($data);

        $fields = [
            'to'           => $deviceToken,
            'notification' => [
                'title' => $title,
                'body'  => $body
            ],
            'data'         => $data,
            'priority'     => 'high',

        ];

        $headers = [
            'Content-Type:application/json',
            'Authorization: key=' . $apiKey
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        if (! $result) {
            throw new Exception("Failed to send push notification! Message:" . curl_error($ch));
        }

        curl_close($ch);

        return true;
    }

    /**
     * Determine which FCM instance API key to use to relay message.
     *
     * @param array $data
     * @return string
     */
    public function determineApiKey(array $data): string
    {
        $instance = in_array($data['type'], [self::NOTIF_CALL_INCOMING, self::NOTIF_CALL_CANCELLED])
            ? 'listener'
            : 'customer';

        return config("firebase.api_key.{$instance}");
    }
}
