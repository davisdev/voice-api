<?php

namespace App\Http\Controllers;

use App\Exceptions\UserException;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws UserException
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->validate([
            'status' => 'required|in:' . implode(',', User::$availableStatuses)
        ]);

        /** @var User $user */
        $user = $request->user();

        if (! ($user->isListener() || $user->isBothApplicationUser())) {
            throw UserException::userIsNotValidListener();
        }

        $user->update($data);

        return response()->json([
            'success' => true,
            'message' => 'User status successfully updated',
            'data'    => []
        ]);
    }
}
