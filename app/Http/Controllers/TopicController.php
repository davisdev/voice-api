<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Http\Resources\TopicCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TopicController extends Controller
{
    /**
     * List all default conversation topics available.
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        return TopicCollection::make(
            Topic::select(['id', 'title', 'description', 'photo'])->get()
        );
    }
}
