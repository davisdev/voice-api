<?php

namespace App\Http\Controllers;

use App\Exceptions\Auth\AuthenticationException;
use App\Exceptions\Auth\VerificationException;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\VerifyRequest;
use App\Models\User;
use App\Services\Contracts\SmsService;
use App\Services\PhoneNumberService;
use App\Utils\Code;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @var SmsService
     */
    private SmsService $sms;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->sms = app(SmsService::class);
    }

    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $typeProvided = $request->get('type');
        $phoneNumber = PhoneNumberService::optimizePhoneNumber(
            $request->get('phone_number')
        );

        $user = User::firstWhere(['phone_number' => $phoneNumber]);

        if (! $user && $typeProvided === User::TYPE_LISTENER) {
            throw AuthenticationException::registrationThroughListenerAppIsNotAllowed();
        }

        if ($user && $user->type !== $typeProvided) {
            throw AuthenticationException::typeInRequestDoesNotMatchUserType();
        }

        if (! $user) {
            $user = User::create([
                'phone_number' => $phoneNumber,
                'type'         => User::TYPE_CUSTOMER
            ]);
        }

        $verificationCode = app(Code::class)->generate();

        $user->update([
            'verification_code' => Hash::make($verificationCode)
        ]);

        $this->sms->send($phoneNumber, $verificationCode);

        return response()->json([
            'success' => true,
            'message' => 'Code has been sent successfully',
            'data'    => []
        ], Response::HTTP_OK);
    }

    /**
     * @param VerifyRequest $request
     * @return JsonResponse
     * @throws VerificationException
     */
    public function verify(VerifyRequest $request): JsonResponse
    {
        $user = User::firstWhere(
            'phone_number',
            $phoneNumber = $request->phone_number
        );

        if (! $user->verification_code) {
            throw VerificationException::verificationCodeIsNotSet();
        }

        $verificationCorrect = $user->isVerificationCodeCorrect(
            $request->get('verification_code'),
        );

        if (! $verificationCorrect) {
            $user->resetVerificationCode();

            throw VerificationException::incorrectVerificationCode();
        }

        $user->resetVerificationCode();

        $token = $user->createToken($phoneNumber)->plainTextToken;

        return response()->json([
            'success' => true,
            'message' => 'User successfully verified',
            'data'    => [
                'token' => $token
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function resend(Request $request): JsonResponse
    {
        $request
            ->merge(['phone_number' => $phoneNumber = PhoneNumberService::optimizePhoneNumber($request->get('phone_number'))])
            ->validate(['phone_number' => 'required|string|exists:users,phone_number']);

        $user = User::firstWhere('phone_number', $phoneNumber);

        $this->sms->send(
            $phoneNumber,
            $newCode = app(Code::class)->generate()
        );

        $user->forceFill([
            'verification_code' => Hash::make($newCode)
        ])->save();

        return response()->json([
            'success' => true,
            'message' => 'Verification code resent',
            'data'    => []
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'success' => true,
            'message' => 'Successfully logged out',
            'data'    => []
        ]);
    }
}
