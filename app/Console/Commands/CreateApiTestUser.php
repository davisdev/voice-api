<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateApiTestUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test-user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create test user just to test it with Postman';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (! app()->environment('local')) {
            $this->error('Only allowed to run in development environment!');

            return -1;
        }

        if (User::firstWhere('email', 'davisnaglis2@gmail.com')) {
            $this->info('User already exists!');

            return 0;
        }

        $user = User::factory()->create([
            'email' => 'davisnaglis2@gmail.com',
            'password' => Hash::make('password')
        ]);

        $user->createToken('test', ['admin-access']);

        $this->info('All done!');

        return 0;
    }
}
