<?php

namespace App\Contracts;

interface PaymentContract
{
    /**
     * Get amount from payment.
     *
     * @return int
     */
    public function amount(): int;
}
