<?php

namespace App\Models;

use App\Exceptions\ConversationException;
use App\Exceptions\UserException;
use App\Exceptions\WalletException;
use App\Notifications\ResetAdminPassword;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $age
 * @property string|null $phone_number
 * @property string|null $gender
 * @property string $type
 * @property string|null $verification_code
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereVerificationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User onlyAdmins()
 * @method static \Illuminate\Database\Eloquent\Builder|User onlyNormal()
 * @method static \Illuminate\Database\Eloquent\Builder|User withoutTokens()
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $photo
 * @property string|null $bio
 * @property-read string $full_name
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoto($value)
 * @property string|null $stripe_id
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $trial_ends_at
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCardLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTrialEndsAt($value)
 * @property float|null $rating
 * @property string|null $device_token
 * @property-read Collection|\App\Models\Rating[] $givenRatings
 * @property-read int|null $given_ratings_count
 * @property-read Collection|\App\Models\Rating[] $receivedRatings
 * @property-read int|null $received_ratings_count
 * @property-read Collection|\App\Models\Wallet[] $wallets
 * @property-read int|null $wallets_count
 * @method static \Illuminate\Database\Eloquent\Builder|User customers()
 * @method static \Illuminate\Database\Eloquent\Builder|User listeners()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeviceToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRating($value)
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @property string $status
 * @method static \Illuminate\Database\Eloquent\Builder|User online()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @property string $date_of_birth
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDateOfBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFullName($value)
 * @property-read Collection|\App\Models\Device[] $devices
 * @property-read int|null $devices_count
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 */
class User extends Authenticatable implements HasMedia
{
    use HasFactory, Notifiable, HasApiTokens, InteractsWithMedia, SoftDeletes;

    const TYPE_CUSTOMER = 'customer';
    const TYPE_LISTENER = 'listener';
    const TYPE_BOTH = 'both';

    const STATUS_ONLINE = 'online';
    const STATUS_ON_CALL = 'on-call';
    const STATUS_OFFLINE = 'offline';

    public static array $availableTypes = [
        self::TYPE_CUSTOMER,
        self::TYPE_LISTENER,
        self::TYPE_BOTH
    ];

    public static array $availableStatuses = [
        self::STATUS_ONLINE,
        self::STATUS_ON_CALL,
        self::STATUS_OFFLINE
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'email',
        'photo',
        'phone_number',
        'verification_code',
        'device_token_customer',
        'device_token_listener',
        'type',
        'status',
        'rating',
        'date_of_birth',
        'gender',
        'bio',
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'verification_code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'age'               => 'integer',
        'rating'            => 'float'
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'date_of_birth'
    ];

    public function devices(): HasMany
    {
        return $this->hasMany(Device::class);
    }

    public function wallets(): HasMany
    {
        return $this->hasMany(Wallet::class);
    }

    /**
     * Ratings given to conversation participant.
     *
     * @return HasMany
     */
    public function givenRatings(): HasMany
    {
        return $this->hasMany(Rating::class, 'reviewer_id');
    }

    /**
     * Ratings received from conversation participant.
     *
     * @return HasMany
     */
    public function receivedRatings(): HasMany
    {
        return $this->hasMany(Rating::class, 'recipient_id');
    }

    /**
     * @return Device
     */
    public function getActiveDevice(): Device
    {
        return $this->devices()->where('is_active', true)->first();
    }

    /**
     * Generate a token name taking into account that its name
     * consists of device ID that user is creating it for.
     */
    public function generateTokenName(string $deviceID): string
    {
        return "user_{$this->id}_device_{$deviceID}";
    }

    /**
     * Recalculate user's average rating after rating is received.
     */
    public function recalculateRating(): void
    {
        $this->update([
            'rating' => $this->fresh()->receivedRatings()->avg('rating')
        ]);
    }

    /**
     * @param Conversation $conversation
     * @return bool
     */
    public function haveBeenRatedIn(Conversation $conversation): bool
    {
        return $conversation->ratings()->where('recipient_id', $this->id)->exists();
    }

    /**
     * Retrieve wallet for specific currency.
     *
     * @param string $currency
     * @return Wallet|null
     */
    public function getWallet(string $currency): ?Wallet
    {
        $wallet = $this->wallets()->firstWhere('currency', $currency);

        return $wallet ?? null;
    }

    /**
     * Create unique wallet for given currency.
     *
     * @param string $currency
     * @param int $amount
     * @return Wallet
     * @throws WalletException
     */
    public function addWallet(string $currency, int $amount = 0): Wallet
    {
        if ($this->wallets()->where('currency', $currency)->exists()) {
            throw WalletException::alreadyExists($currency);
        }

        return $this->wallets()->create([
            'currency' => $currency,
            'balance'  => $amount
        ]);
    }

    /**
     * @param string $codeFromRequest
     * @return bool
     */
    public function isVerificationCodeCorrect(string $codeFromRequest): bool
    {
        return Hash::check($codeFromRequest, $this->verification_code);
    }

    /**
     * @return bool
     */
    public function resetVerificationCode(): bool
    {
        return $this->update([
            'verification_code' => null
        ]);
    }

    /**
     * @param string $value
     * @throws UserException
     */
    public function setTypeAttribute(string $value): void
    {
        if (! in_array($value, self::$availableTypes)) {
            throw UserException::givenTypeInvalid(allowedTypes: self::$availableTypes);
        }

        $this->attributes['type'] = $value;
    }

    /**
     * @param string $value
     * @throws UserException
     */
    public function setStatusAttribute(string $value): void
    {
        if (! in_array($value, self::$availableStatuses)) {
            throw UserException::givenStatusInvalid(allowedStatuses: self::$availableStatuses);
        }

        $this->attributes['status'] = $value;
    }

    /**
     * @param string $value
     */
    public function setDateOfBirthAttribute(string $value)
    {
        $this->attributes['date_of_birth'] = Carbon::createFromFormat('d.m.Y', $value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getDateOfBirthAttribute($value): string
    {
        return Carbon::parse($value)->format('d.m.Y');
    }

    /**
     * @return int|null
     */
    public function getAgeAttribute(): ?int
    {
        return $this->date_of_birth
            ? Carbon::parse($this->date_of_birth)->diffInYears(now())
            : null;
    }

    /**
     * @param string|null $value
     * @throws UserException
     */
    public function setEmailAttribute(?string $value): void
    {
        // Allow for nullable.
        if (is_null($value)) {
            $this->attributes['email'] = null;

            return;
        }

        if (! filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw UserException::invalidEmailFormat();
        }

        $this->attributes['email'] = $value;
    }

    /**
     * @param string $value
     * @throws UserException
     */
    public function setGenderAttribute(string $value): void
    {
        if (! in_array($value, ['male', 'female'])) {
            throw UserException::givenGenderInvalid(allowedGenders: ['male', 'female']);
        }

        $this->attributes['gender'] = $value;
    }

    /**
     * Clear all user tokens (without administrative tokens).
     */
    public function deleteTokens(): void
    {
        $this->tokens()->where('abilities', '!=', 'admin-access')->delete();
    }

    /**
     * Check for given ability (outside of middleware context).
     *
     * @param string $ability
     * @return bool
     */
    public function hasAbility(string $ability): bool
    {
        if (! $this->tokens()->count()) {
            return false;
        }

        return (bool) $this->tokensWithAbilities()
            ->filter(
                fn(PersonalAccessToken $token) => in_array($ability, array_values($token->abilities))
            )->count();
    }

    /**
     * Get tokens who have any kind of ability except for "*".
     *
     * @return Collection
     */
    public function tokensWithAbilities(): Collection
    {
        return $this->tokens()->where('abilities', '!=', '["*"]')->get();
    }

    /**
     * Scope only administrators.
     *
     * @param $query
     * @return mixed
     */
    public function scopeOnlyAdmins($query)
    {
        return $query->whereHas('tokens', function ($query) {
            $query->where('abilities', '["admin-access"]');
        });
    }

    /**
     * Scope only normal users.
     *
     * @param $query
     * @return mixed
     */
    public function scopeOnlyNormal($query)
    {
        return $query->whereHas('tokens', function ($query) {
            $query->where('abilities', '["*"]');
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public static function scopeCustomers($query)
    {
        return $query->whereIn('type', [User::TYPE_BOTH, User::TYPE_CUSTOMER]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public static function scopeListeners($query)
    {
        return $query->whereIn('type', [User::TYPE_BOTH, User::TYPE_LISTENER]);
    }

    /**
     * @param $query
     * @return mixed
     */
    public static function scopeOnline($query)
    {
        return $query->where('status', self::STATUS_ONLINE);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithoutTokens($query)
    {
        return $query->whereDoesntHave('tokens');
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetAdminPassword($token));
    }

    /**
     * @return bool
     */
    public function resetProfileImage(): bool
    {
        $this->clearMediaCollection('photo');

        $this->update([
            'photo' => $this->createDefaultProfilePhoto()
        ]);

        return true;
    }

    /**
     * Register 'photo' collection for profile.
     */
    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('photo')
            ->singleFile();
    }

    /**
     * @return mixed
     */
    private function createDefaultProfilePhoto(): string
    {
        return "https://www.gravatar.com/avatar/" . md5(strtolower(trim($this->email))) . "&s=64";
    }

    /**
     * Determine is user is active on system (can be on call currently).
     *
     * @return bool
     * @throws Exception
     */
    public function isOnline(): bool
    {
        return in_array($this->status, [self::STATUS_ONLINE, self::STATUS_ON_CALL]);
    }

    /**
     * @return bool
     */
    public function isAvailableForCall(): bool
    {
        return $this->status === self::STATUS_ONLINE;
    }

    /**
     * @return bool
     */
    public function isBothApplicationUser(): bool
    {
        return $this->type === self::TYPE_BOTH;
    }

    /**
     * @return bool
     */
    public function isCustomer(): bool
    {
        return $this->type === self::TYPE_CUSTOMER;
    }

    /**
     * @return bool
     */
    public function isListener(): bool
    {
        return $this->type === self::TYPE_LISTENER;
    }

    /**
     * Get the correct user type that should be set.
     *
     * @param string $phoneNumber
     * @param string $typeProvided
     * @return string
     */
    public static function getCorrectType(string $phoneNumber, string $typeProvided): string
    {
        $user = User::firstWhere('phone_number', $phoneNumber);

        if (! $user) {
            return $typeProvided;
        }

        // When user isn't using both app's and we want to set a new type for user that is not
        // current user type, we set it to 'both'.
        if ($user->isBothApplicationUser() || $typeProvided !== $user->type) {
            return self::TYPE_BOTH;
        }

        // Completely new users get assigned the type
        // they get from the appropriate app.
        return $typeProvided;
    }

    /**
     * @param User $listener
     * @param Topic|null $topic
     * @return Conversation
     * @throws ConversationException
     */
    public function startConversationWith(User $listener, Topic $topic = null): Conversation
    {
        return Conversation::start($this, $listener, $topic);
    }

    /**
     * @return bool
     */
    public function hasCredits(): bool
    {
        return true;
    }
}
