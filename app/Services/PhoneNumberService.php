<?php

namespace App\Services;

class PhoneNumberService
{
    /**
     * Normalize phone number and add country code.
     *
     * @param string $number
     * @return string
     */
    public static function optimizePhoneNumber(string $number): string
    {
        return ! str_starts_with($number, '+')
            ? '+371' . $number
            : $number;
    }
}
