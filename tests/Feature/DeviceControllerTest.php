<?php

namespace Tests\Feature;

use App\Models\Device;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class DeviceControllerTest extends TestCase
{
    use RefreshDatabase;

    private Authenticatable $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = Sanctum::actingAs(User::factory()->create());
    }

    /** @test */
    public function if_device_exists_it_gets_restored_and_correct_user_is_assigned_to_it()
    {
        $softDeletedDevice = Device::factory()->create([
            'user_id'    => User::factory(),
            'deleted_at' => now()
        ]);

        $this->assertTrue($softDeletedDevice->trashed());

        $this->postJson(route('device.store'), $softDeletedDevice->only(['device_id', 'type', 'user_id']))
            ->assertSuccessful()
            ->assertJson([
                'message' => 'Device restored and user ID assigned'
            ]);

        // Pulling fresh device as it got updated
        $device = $softDeletedDevice->fresh();

        $this->assertFalse($device->trashed());
        $this->assertEquals($this->user->id, $device->user_id);
    }

    /** @test */
    public function if_device_does_not_exist_it_gets_created_for_current_user()
    {
        $this->assertDatabaseCount('devices', 0);

        $this->postJson(route('device.store'), Device::factory()->make()->toArray())
            ->assertSuccessful()
            ->assertJson([
                'message' => 'Device created successfully'
            ]);

        // Pulling fresh device as it got updated
        $device = Device::first();

        // No matter what user_id gets passed in, currently signed in user gets assigned
        // (since single endpoint is both for create and update operation on device).
        $this->assertEquals($this->user->id, $device->user_id);
    }
}
