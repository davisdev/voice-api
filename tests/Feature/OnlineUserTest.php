<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class OnlineUserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var Authenticatable
     */
    private Authenticatable $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = Sanctum::actingAs(User::factory()->create());
    }

    /** @test */
    public function list_of_online_users_can_be_seen()
    {
        $users = User::factory()->count($onlineUserCount = 11)->create(['type' => User::TYPE_LISTENER]);
        $users->each(function (User $user) {
            $user->update(['status' => User::STATUS_ONLINE]);
        });

        $this->getJson(route('online.listeners'))
            ->assertJsonCount($onlineUserCount, 'data');
    }

    /** @test */
    public function user_can_be_updated_to_appear_online()
    {
        $this->assertFalse($this->user->isOnline());

        $this->postJson(route('online.broadcast'))
            ->assertSuccessful();

        $this->assertTrue($this->user->isOnline());
    }
}
