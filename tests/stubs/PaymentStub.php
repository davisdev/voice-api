<?php

namespace Tests\stubs;

use App\Contracts\PaymentContract;

class PaymentStub implements PaymentContract
{
    private int $amount;

    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }
}
