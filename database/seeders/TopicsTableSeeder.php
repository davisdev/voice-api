<?php

namespace Database\Seeders;

use App\Models\Topic;
use Illuminate\Database\Seeder;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topics = [
            // TODO Need to pre-populate with some topics
        ];

        collect($topics)->each(function ($topic) {
            Topic::firstOrCreate($topic);
        });

        $this->command->info('Default topics seeded!');
    }
}
